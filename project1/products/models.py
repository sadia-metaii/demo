from django.db import models


class Car(models.Model):
	cname = models.CharField(max_length=400)
	ctype = models.CharField(max_length=400)
	color = models.CharField(max_length=400)

	def __str__(self):
		return "Name:"+self.cname+ " " + "Type:"+self.ctype+ "Color:"+self.color
