from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse

def index(request):
    return HttpResponse("<h1>Hello World</h1>");

def box(request):
     return render(request, 'products/box.html')

def ajaxPost(request):
     return render(request, 'products/ajax_post.html')
def listajax(request):
    return JsonResponse({'foo':'bar'})

def insert(request):
    text = request.POST.get('came')
    #a=Car()
    #a.cname=request.POST.get("cname")
    #a.ctype=request.POST.get("ctype")
    #a.ctype=request.POST.get("ctype")
    #a.save()
    return HttpResponse(text)
