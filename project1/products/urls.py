from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^box',views.box),
    url(r'^ajaxPost',views.ajaxPost),
    url(r'^listajax',views.listajax),
    url(r'^insert',views.insert),

]
